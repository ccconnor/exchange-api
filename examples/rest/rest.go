package main

import (
	"fmt"
	bitmex "gitee.com/ccconnor/exchange-api/exchange/bitmex/rest"
	okex "gitee.com/ccconnor/exchange-api/exchange/okex/rest"
)

func getBitMexOrder() {
	endpoint := "https://testnet.bitmex.com"
	apiKey := "jAwOyiesXELlb-lHi3j3zhkp"
	secretKey := "ydYMBfO0dYcFwDonwGZuwyJ3debrLJMonkL23BNLFZC8mxJZ"
	bitmexRestCli := bitmex.NewClient(endpoint, apiKey, secretKey)

	filter := map[string]interface{}{"workingIndicator": true}
	params := &bitmex.GetOrderParam{Filter: filter}
	orderList, err := bitmexRestCli.GetOrder(params)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("Got %d order(s)\n", len(orderList))
	for _, v := range orderList {
		fmt.Printf("%+v\n", *v)
	}
}

func getOkexPosition() {
	apikey := "27ad80b0-4a1c-40c5-a24d-dc2140990c2a"
	secretkey := "9415A393A5A679E400FB0978168F4AE1"
	password := "123456"
	endpoint := "https://testnet.okex.me"
	restHandler := okex.NewClient(endpoint, apikey, secretkey, password)
	res, err := restHandler.Futures.GetAllPositions()
	if err != nil {
		fmt.Println(err)
		return
	}
	if !res.Result {
		fmt.Println("get position failed")
		return
	}
	fmt.Printf("%+v\n", res)
}

func main() {
	getOkexPosition()
}
