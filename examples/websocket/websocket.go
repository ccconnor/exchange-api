package main

import (
	"fmt"
	"gitee.com/ccconnor/exchange-api/exchange/bitmex/types"
	"gitee.com/ccconnor/exchange-api/exchange/bitmex/websocket"
	"log"
	"time"
)

func main() {
	endpoint := "wss://testnet.bitmex.com/realtime"
	apiKey := "cihb_hjaTRycwJ9ML4UEIGgR"
	secretKey := "ZTbKCf47aftV6PvoX7QFvU1EX6wsdzsAc7L-_K-JjHrIrxwv"
	topics := []string{"orderBookL2_25:XBTUSD"}

	msgHandler := func(message *types.Message) {
		switch message.Table {
		case "trade":
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.Trade)))
		case "orderBookL2":
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.OrderBook)))
		case "position":
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.Position)))
		case "execution":
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.Execution)))
		case "orderBookL2_25":
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.OrderBook)))
		case "order":
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.Order)))
		}
	}

	bitmexWebsocket := websocket.NewClient(endpoint, apiKey, secretKey, topics, msgHandler, false)
	err := bitmexWebsocket.Connect()
	if err != nil {
		fmt.Println(err)
		return
	}
	log.Println("connected to websocket")

	time.Sleep(10 * time.Second)

	bitmexWebsocket.Close()
}
