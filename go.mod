module gitee.com/ccconnor/exchange-api

go 1.17

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/gorilla/websocket v1.4.1
	github.com/iancoleman/strcase v0.0.0-20191112232945-16388991a334
	github.com/kraymond37/simplehttp v1.0.4
	github.com/mitchellh/mapstructure v1.3.2
	github.com/shopspring/decimal v1.2.0
)

require (
	github.com/andybalholm/cascadia v1.1.0 // indirect
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2 // indirect
)
