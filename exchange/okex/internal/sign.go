package internal

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
)

func Sign(secret, method, path, timestamp, payload string) string {
	toSign := timestamp + method + path + payload
	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(toSign))
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}
