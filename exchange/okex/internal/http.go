package internal

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"reflect"
	"time"

	"github.com/kraymond37/simplehttp"
	"github.com/mitchellh/mapstructure"
)

type Client struct {
	apiKey    string
	secretKey string
	password  string
	client    *simplehttp.Client
}

func NewClient(endpoint, apiKey, secretKey, password string) *Client {
	c := simplehttp.NewClient(endpoint)
	if c == nil {
		fmt.Println("new client failed")
		return nil
	}
	return &Client{apiKey: apiKey, secretKey: secretKey, password: password, client: c}
}

func (c Client) makeAuthHeader(method, path string, params map[string]interface{}) http.Header {
	var query string
	var payload []byte
	if method == "GET" {
		query = simplehttp.MapToUrlValues(params).Encode()
	} else {
		payload, _ = json.Marshal(params)
	}
	reqURL := url.URL{Path: path, RawQuery: query}

	timestamp := time.Now().UTC().Format(time.RFC3339)
	signature := Sign(c.secretKey, method, reqURL.RequestURI(), timestamp, string(payload))

	header := http.Header{}
	header.Set("OK-ACCESS-KEY", c.apiKey)
	header.Set("OK-ACCESS-SIGN", signature)
	header.Set("OK-ACCESS-TIMESTAMP", timestamp)
	header.Set("OK-ACCESS-PASSPHRASE", c.password)

	return header
}

func (c Client) RequestMap(method, path string, params map[string]interface{}, resp interface{}, needSign bool) (err error) {
	var header http.Header
	if needSign {
		header = c.makeAuthHeader(method, path, params)
	}

	var body []byte
	switch method {
	case "GET":
		body, err = c.client.Get(path, params, header)
	case "POST":
		body, err = c.client.PostJson(path, params, header)
	default:
		err = fmt.Errorf("method not supported")
	}
	if err != nil {
		log.Println("http request failed", string(body))
		return err
	}

	err = json.Unmarshal(body, resp)
	if err != nil {
		return err
	}

	return nil
}

func (c Client) RequestStruct(method, path string, param, resp interface{}, needSign bool) (err error) {
	var paramsMap map[string]interface{}
	if param != nil && !reflect.ValueOf(param).IsNil() {
		err := mapstructure.Decode(param, &paramsMap)
		if err != nil {
			return err
		}
	}
	return c.RequestMap(method, path, paramsMap, resp, needSign)
}
