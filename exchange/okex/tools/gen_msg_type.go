package main

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/iancoleman/strcase"
	"log"
	"net/http"
	"sort"
	"strings"
)

type TypeInfo struct {
	selector    string
	description string
	reqOrResp   string
}

type ItemDefines struct {
	itemName string
	itemType string
	itemJson string
	itemDesc string
}

type ItemList []ItemDefines

func (l ItemList) Len() int {
	return len(l)
}

func (l ItemList) Less(i, j int) bool {
	return l[i].itemName < l[j].itemName
}

func (l ItemList) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

var typeInfoList = map[string]TypeInfo{
	"Instrument": {
		selector:    "body > div > div.book-body > div > div.page-wrapper > div > section > div:nth-child(1896) > div.api-method-definition > table > tbody > tr",
		description: "合约信息",
		reqOrResp:   "response",
	},
	"Order": {
		selector:    "body > div > div.book-body > div > div.page-wrapper > div > section > div:nth-child(1136) > div.api-method-definition > table > tbody > tr",
		description: "交割合约订单信息",
		reqOrResp:   "response",
	},
	"FixedPosition": {
		selector:    "body > div > div.book-body > div > div.page-wrapper > div > section > div:nth-child(1641) > div.api-method-definition > table:nth-child(5) > tbody > tr",
		description: "交割合约逐仓仓位信息",
		reqOrResp:   "response",
	},
	"CrossedPosition": {
		selector:    "body > div > div.book-body > div > div.page-wrapper > div > section > div:nth-child(1641) > div.api-method-definition > table:nth-child(3) > tbody > tr",
		description: "交割合约全仓仓位信息",
		reqOrResp:   "response",
	},
	"TableFixedPosition": {
		selector:    "body > div > div.book-body > div > div.page-wrapper > div > section > div:nth-child(2952) > div.api-method-definition > table > tbody > tr",
		description: "websocket交割合约用户持仓频道逐仓返回参数",
		reqOrResp:   "response",
	},
	"TableCrossedPosition": {
		selector:    "body > div > div.book-body > div > div.page-wrapper > div > section > div:nth-child(2953) > div.api-method-definition > table > tbody > tr",
		description: "websocket交割合约用户持仓频道全仓返回参数",
		reqOrResp:   "response",
	},
	"TableOrder": {
		selector:    "body > div > div.book-body > div > div.page-wrapper > div > section > div:nth-child(2333) > div.api-method-definition > table > tbody > tr",
		description: "websocket交割合约用户交易频道返回参数",
		reqOrResp:   "response",
	},
	"NewOrderParam": {
		selector:    "body > div > div.book-body > div > div.page-wrapper > div > section > table:nth-child(1708) > tbody > tr",
		description: "交割合约下单请求参数",
		reqOrResp:   "request",
	},
	"GetOrdersParam": {
		selector:    "body > div > div.book-body > div > div.page-wrapper > div > section > table:nth-child(1754) > tbody > tr",
		description: "交割合约订单列表请求参数",
		reqOrResp:   "request",
	},
	"CrossedAccount": {
		selector:    "body > div > div.book-body > div > div.page-wrapper > div > section > div:nth-child(1659) > div.api-method-definition > table:nth-child(3) > tbody > tr",
		description: "交割合约全仓账户信息",
		reqOrResp:   "response",
	},
	"FixedAccount": {
		selector:    "body > div > div.book-body > div > div.page-wrapper > div > section > div:nth-child(1659) > div.api-method-definition > table:nth-child(5) > tbody > tr",
		description: "交割合约逐仓账户信息",
		reqOrResp:   "response",
	},
}

func generateType(whichType string) {
	typeInfo, found := typeInfoList[whichType]
	if !found {
		log.Fatalln("先定义类型信息")
	}
	switch typeInfo.reqOrResp {
	case "request":
		generateRequestType(whichType, typeInfo)
	case "response":
		generateResponseType(whichType, typeInfo)
	}
}

func generateResponseType(whichType string, typeInfo TypeInfo) {
	res, err := http.Get("https://www.okex.me/docs/zh")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	var itemList ItemList
	doc.Find(typeInfo.selector).Each(func(i int, s *goquery.Selection) {
		var items [3]string
		s.Find("td").Each(func(i int, selection *goquery.Selection) {
			items[i] = selection.Text()
		})

		var itemDefines ItemDefines
		itemDefines.itemName = strcase.ToCamel(items[0])
		itemDefines.itemJson = items[0]
		itemDefines.itemDesc = items[2]
		switch items[1] {
		case "String":
			itemDefines.itemType = "string"
		default:
			itemDefines.itemType = items[1]
		}
		if strings.Contains(itemDefines.itemDesc, "时间") {
			itemDefines.itemType = "time.Time"
		}

		itemList = append(itemList, itemDefines)
	})

	sort.Sort(itemList)

	fmt.Printf("//%s\n", typeInfo.description)
	fmt.Printf("type %s struct {\n", whichType)
	for _, item := range itemList {
		fmt.Printf("\t%s %s `json:\"%s\"` //%s\n", item.itemName, item.itemType, item.itemJson, item.itemDesc)
	}
	fmt.Println("}")
}

func generateRequestType(whichType string, typeInfo TypeInfo) {
	res, err := http.Get("https://www.okex.me/docs/zh")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	var itemList ItemList
	doc.Find(typeInfo.selector).Each(func(i int, s *goquery.Selection) {
		var items [4]string
		s.Find("td").Each(func(i int, selection *goquery.Selection) {
			if i < 4 {
				items[i] = selection.Text()
			} else {
				fmt.Println(selection.Text())
			}
		})

		var itemDefines ItemDefines
		itemDefines.itemName = strcase.ToCamel(items[0])
		itemDefines.itemJson = items[0]
		itemDefines.itemDesc = items[3]
		switch items[1] {
		case "String":
			itemDefines.itemType = "string"
		default:
			itemDefines.itemType = items[1]
		}

		if items[2] == "否" {
			itemDefines.itemType = "*" + itemDefines.itemType
			itemDefines.itemJson += ",omitempty"
		}

		itemList = append(itemList, itemDefines)
	})

	//sort.Sort(itemList)

	fmt.Printf("//%s\n", typeInfo.description)
	fmt.Printf("type %s struct {\n", whichType)
	for _, item := range itemList {
		fmt.Printf("\t%s %s `mapstructure:\"%s\"` //%s\n", item.itemName, item.itemType, item.itemJson, item.itemDesc)
	}
	fmt.Println("}")
}

func main() {
	generateType("FixedAccount")
}
