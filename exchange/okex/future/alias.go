package future

import (
	"gitee.com/ccconnor/exchange-api/exchange/okex/future/internal/api"
	"gitee.com/ccconnor/exchange-api/exchange/okex/future/internal/types"
)

// 公共类型
type (
	Client         = api.Client
	Order          = types.Order
	Position       = types.Position
	FixedAccount   = types.FixedAccount
	CrossedAccount = types.CrossedAccount
)

// 推送类型
type (
	TableOrder      = types.TableOrder
	TablePosition   = types.TablePosition
	TableDepth5     = types.TableDepth5
	TablePriceRange = types.TablePriceRange
)

// api参数类型
type (
	NewOrderParam  = api.NewOrderParam
	GetOrdersParam = api.GetOrdersParam
	GetDepthParam  = api.GetDepthParam
)

// api返回类型
type (
	NewOrderResp        = api.NewOrderResp
	CancelOrderResp     = api.CancelOrderResp
	GetOrdersResp       = api.GetOrdersResp
	GetAllPositionsResp = api.GetAllPositionsResp
	GetPositionResp     = api.GetPositionResp
	GetDepthResp        = api.GetDepthResp
)
