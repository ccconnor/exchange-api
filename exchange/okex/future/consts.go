package future

const (
	SideBuy  = "buy"
	SideSell = "sell"
)

const (
	OpenLong   = 1 // 开多
	OpenShort  = 2 // 开空
	CloseLong  = 3 // 平多
	CloseShort = 4 // 平空
)

const (
	OrderStateFailed          = -2 // 失败
	OrderStateCanceled        = -1 // 撤单成功
	OrderStatePending         = 0  // 等待成交
	OrderStatePartiallyFilled = 1  // 部分成交
	OrderStateFilled          = 2  // 完全成交
	OrderStateNew             = 3  // 下单中
	OrderStateCancel          = 4  // 撤单中
	OrderStateUnfinished      = 6  // 未完成（等待成交+部分成交）仅查询用
	OrderStateFinished        = 7  // 已完成（撤单成功+完全成交）仅查询用
)
