package api

import "gitee.com/ccconnor/exchange-api/exchange/okex/future/internal/types"

func (c Client) GetFixedAccount(underlying string) (resp *types.FixedAccount, err error) {
	path := "/api/futures/v3/accounts/" + underlying
	err = c.HTTPClient.RequestMap("GET", path, nil, &resp, true)
	return
}

func (c Client) GetCrossedAccount(underlying string) (resp *types.CrossedAccount, err error) {
	path := "/api/futures/v3/accounts/" + underlying
	err = c.HTTPClient.RequestMap("GET", path, nil, &resp, true)
	return
}
