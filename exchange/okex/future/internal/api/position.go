package api

import (
	"fmt"

	"gitee.com/ccconnor/exchange-api/exchange/okex/future/internal/types"
)

type GetAllPositionsResp struct {
	Result  bool               `json:"result"`
	Holding [][]types.Position `json:"holding"` // 交割合约第一维不多于1个元素
}

func (c Client) GetAllPositions() (resp *GetAllPositionsResp, err error) {
	path := "/api/futures/v3/position"
	err = c.HTTPClient.RequestMap("GET", path, nil, &resp, true)
	return
}

type GetPositionResp struct {
	Result  bool             `json:"result"`
	Holding []types.Position `json:"holding"` // 交割合约不多于1个元素
}

func (c Client) GetPosition(instrumentID string) (resp *GetPositionResp, err error) {
	path := fmt.Sprintf("/api/futures/v3/%s/position", instrumentID)
	err = c.HTTPClient.RequestMap("GET", path, nil, &resp, true)
	return
}
