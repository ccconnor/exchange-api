package api

import (
	"gitee.com/ccconnor/exchange-api/exchange/okex/future/internal/types"
)

func (c Client) GetInstruments() (resp []*types.Instrument, err error) {
	path := "/api/futures/v3/instruments"
	err = c.HTTPClient.RequestMap("GET", path, nil, &resp, false)
	return
}
