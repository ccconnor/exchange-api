package api

import (
	"fmt"

	"gitee.com/ccconnor/exchange-api/exchange/okex/future/internal/types"
)

type NewOrderResp struct {
	Result       bool   `json:"result"`
	ErrorMessage string `json:"error_message"`
	ErrorCode    string `json:"error_code"`
	ClientOid    string `json:"client_oid"`
	OrderID      string `json:"order_id"`
}

// NewOrderParam 交割合约下单请求参数
type NewOrderParam struct {
	ClientOid    *string  `mapstructure:"client_oid,omitempty"`  // 由您设置的订单ID来式是字母（区分大小写）+数字 或者 纯字母（区分大小写），1-32位字符   （不能重复）
	InstrumentID string   `mapstructure:"instrument_id"`         // 合约ID，如BTC-USD-18021TC-USDT-191227
	MatchPrice   *float64 `mapstructure:"match_price,omitempty"` // 是否以对手价下单:是)，默认为0，当取值为1时，price字段无效。当以对手价下单，order_type只能选择0（普通委托）
	OrderType    *int     `mapstructure:"order_type,omitempty"`  // 参数填数字0：普通委type不填或填0都是普通委托） 1：只做Maker（Post only） 2：全部成交或立即取消（FOK） 3：立即成交并取消剩余（IOC）  4：市价委托
	Price        *float64 `mapstructure:"price,omitempty"`       // 委托价格
	Size         int64    `mapstructure:"size"`                  // 买入或卖出合约的数量（以张计数）
	Type         int      `mapstructure:"type"`                  // 1:开多2:开空3:平多4:平空
}

func (c Client) NewOrder(params *NewOrderParam) (resp *NewOrderResp, err error) {
	path := "/api/futures/v3/order"
	err = c.HTTPClient.RequestStruct("POST", path, params, &resp, true)
	return
}

func (c Client) GetOrder(instrumentID, orderID string) (resp *types.Order, err error) {
	path := fmt.Sprintf("/api/futures/v3/orders/%s/%s", instrumentID, orderID)
	err = c.HTTPClient.RequestMap("GET", path, nil, &resp, true)
	return
}

type CancelOrderResp struct {
	Result       bool   `json:"result"`
	ErrorMessage string `json:"error_message"`
	ErrorCode    string `json:"error_code"`
	ClientOid    string `json:"client_oid"`
	OrderID      string `json:"order_id"`
	InstrumentID string `json:"instrument_id"`
}

func (c Client) CancelOrder(instrumentID, orderID string) (resp *CancelOrderResp, err error) {
	path := fmt.Sprintf("/api/futures/v3/cancel_order/%s/%s", instrumentID, orderID)
	err = c.HTTPClient.RequestMap("POST", path, nil, &resp, true)
	return
}

// GetOrdersParam 交割合约订单列表请求参数
type GetOrdersParam struct {
	InstrumentID string  `mapstructure:"instrument_id"`    // 合约ID，如BTC-USD-180213 ,BTC-USDT-191227
	State        int     `mapstructure:"state"`            // 订单状态-2:失败-1:撤单成功0:等待成交1:部分成交2:完全成交3:下单中4:撤单中6: 未完成（等待成交+部分成交）7:已完成（撤单成功+完全成交）
	After        *string `mapstructure:"after,omitempty"`  // 请求此id之前(更旧的数据)的分页内容，传的值为对应接口的order_id；
	Before       *string `mapstructure:"before,omitempty"` // 请求此id之后(更新的数据)的分页内容，传的值为对应接口的order_id；
	Limit        *int    `mapstructure:"limit,omitempty"`  // 分页返回的结果集数量，最大为100，不填默认返回100条
}

type GetOrdersResp struct {
	Result    bool          `json:"result"`
	OrderInfo []types.Order `json:"order_info"`
}

func (c Client) GetOrders(params GetOrdersParam) (resp *GetOrdersResp, err error) {
	path := fmt.Sprintf("/api/futures/v3/orders/%s", params.InstrumentID)
	err = c.HTTPClient.RequestStruct("GET", path, &params, &resp, true)
	return
}
