package api

import (
	"fmt"
	"time"

	"github.com/shopspring/decimal"
)

type GetDepthParam struct {
	InstrumentID string   `mapstructure:"instrument_id"`   // 合约ID，如BTC-USD-180213,BTC-USDT-191227
	Size         *int     `mapstructure:"size,omitempty"`  // 返回深度数量，最大值可传200，即买卖深度共400条
	Depth        *float64 `mapstructure:"depth,omitempty"` // 按价格合并深度，例如：0.1，0.001
}

type GetDepthResp struct {
	Asks      [][4]decimal.Decimal `json:"asks"`
	Bids      [][4]decimal.Decimal `json:"bids"`
	Timestamp time.Time            `json:"timestamp"`
}

func (c Client) GetDepth(params GetDepthParam) (resp *GetDepthResp, err error) {
	path := fmt.Sprintf("/api/futures/v3/instruments/%s/book", params.InstrumentID)
	err = c.HTTPClient.RequestStruct("GET", path, &params, &resp, true)
	return
}
