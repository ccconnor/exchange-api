package types

import (
	"time"

	"github.com/shopspring/decimal"
)

// Position 交割合约仓位信息
type Position struct {
	CreatedAt             time.Time       `json:"created_at"`               // 创建时间
	InstrumentID          string          `json:"instrument_id"`            // 合约ID，如BTC-USD-180213
	Last                  decimal.Decimal `json:"last"`                     // 最新成交价
	Leverage              decimal.Decimal `json:"leverage"`                 // 杠杆倍数 全仓字段
	LiquidationPrice      decimal.Decimal `json:"liquidation_price"`        // 预估强平价 全仓字段
	LongAvailQty          decimal.Decimal `json:"long_avail_qty"`           // 多仓可平仓数量
	LongAvgCost           decimal.Decimal `json:"long_avg_cost"`            // 开仓平均价
	LongLeverage          decimal.Decimal `json:"long_leverage"`            // 多仓杠杆倍数 逐仓字段
	LongLiquiPrice        decimal.Decimal `json:"long_liqui_price"`         // 多仓强平价格 逐仓字段
	LongMaintMarginRatio  decimal.Decimal `json:"long_maint_margin_ratio"`  // 多仓维持保证金率 逐仓字段
	LongMargin            decimal.Decimal `json:"long_margin"`              // 多仓保证金
	LongMarginRatio       decimal.Decimal `json:"long_margin_ratio"`        // 多仓保证金率 逐仓字段
	LongPnl               decimal.Decimal `json:"long_pnl"`                 // 多仓收益
	LongPnlRatio          decimal.Decimal `json:"long_pnl_ratio"`           // 多仓收益率
	LongQty               decimal.Decimal `json:"long_qty"`                 // 多仓数量
	LongSettledPnl        decimal.Decimal `json:"long_settled_pnl"`         // 多仓已结算收益
	LongSettlementPrice   decimal.Decimal `json:"long_settlement_price"`    // 多仓结算基准价
	LongUnrealisedPnl     decimal.Decimal `json:"long_unrealised_pnl"`      // 多仓未实现盈亏
	MarginMode            string          `json:"margin_mode"`              // 账户类型 逐仓：fixed, 全仓：crossed
	RealisedPnl           decimal.Decimal `json:"realised_pnl"`             // 已实现盈余
	ShortAvailQty         decimal.Decimal `json:"short_avail_qty"`          // 空仓可平仓数量
	ShortAvgCost          decimal.Decimal `json:"short_avg_cost"`           // 开仓平均价
	ShortLeverage         decimal.Decimal `json:"short_leverage"`           // 空仓杠杆倍数 逐仓字段
	ShortLiquiPrice       decimal.Decimal `json:"short_liqui_price"`        // 空仓强平价格 逐仓字段
	ShortMaintMarginRatio decimal.Decimal `json:"short_maint_margin_ratio"` // 空仓维持保证金率 逐仓字段
	ShortMargin           decimal.Decimal `json:"short_margin"`             // 空仓保证金
	ShortMarginRatio      decimal.Decimal `json:"short_margin_ratio"`       // 空仓保证金率 逐仓字段
	ShortPnl              decimal.Decimal `json:"short_pnl"`                // 空仓收益
	ShortPnlRatio         decimal.Decimal `json:"short_pnl_ratio"`          // 空仓收益率
	ShortQty              decimal.Decimal `json:"short_qty"`                // 空仓数量
	ShortSettledPnl       decimal.Decimal `json:"short_settled_pnl"`        // 空仓已结算收益
	ShortSettlementPrice  decimal.Decimal `json:"short_settlement_price"`   // 空仓结算基准价
	ShortUnrealisedPnl    decimal.Decimal `json:"short_unrealised_pnl"`     // 空仓未实现盈亏
	UpdatedAt             time.Time       `json:"updated_at"`               // 最近一次加减仓的更新时间
}

// TablePosition websocket交割合约用户持仓频道返回参数
type TablePosition struct {
	CreatedAt             time.Time       `json:"created_at"`               // 创建时间
	InstrumentID          string          `json:"instrument_id"`            // 合约ID
	Last                  decimal.Decimal `json:"last"`                     // 最新成交价
	Leverage              decimal.Decimal `json:"leverage"`                 // 杠杆倍数 全仓字段
	LiquidationPrice      decimal.Decimal `json:"liquidation_price"`        // 预估强平价 全仓字段
	LongAvailQty          decimal.Decimal `json:"long_avail_qty"`           // 多仓可平仓数量
	LongAvgCost           decimal.Decimal `json:"long_avg_cost"`            // 开仓平均价
	LongLeverage          decimal.Decimal `json:"long_leverage"`            // 多仓杠杆倍数 逐仓字段
	LongLiquiPrice        decimal.Decimal `json:"long_liqui_price"`         // 多仓强平价格 逐仓字段
	LongMaintMarginRatio  decimal.Decimal `json:"long_maint_margin_ratio"`  // 多仓维持保证金率 逐仓字段
	LongMargin            decimal.Decimal `json:"long_margin"`              // 多仓保证金
	LongMarginRatio       decimal.Decimal `json:"long_margin_ratio"`        // 多仓保证金率 逐仓字段
	LongOpenOutstanding   decimal.Decimal `json:"long_open_outstanding"`    // 多仓开仓冻结张数
	LongPnl               decimal.Decimal `json:"long_pnl"`                 // 多仓收益
	LongPnlRatio          decimal.Decimal `json:"long_pnl_ratio"`           // 多仓收益率
	LongQty               decimal.Decimal `json:"long_qty"`                 // 多仓数量
	LongSettledPnl        decimal.Decimal `json:"long_settled_pnl"`         // 多仓已结算收益
	LongSettlementPrice   decimal.Decimal `json:"long_settlement_price"`    // 结算基准价
	LongUnrealisedPnl     decimal.Decimal `json:"long_unrealised_pnl"`      // 多仓未实现盈亏
	MarginMode            string          `json:"margin_mode"`              // 账户类型 逐仓:fixed, 全仓：crossed
	RealisedPnl           decimal.Decimal `json:"realised_pnl"`             // 已实现盈余
	ShortAvailQty         decimal.Decimal `json:"short_avail_qty"`          // 空仓可平仓数量
	ShortAvgCost          decimal.Decimal `json:"short_avg_cost"`           // 开仓平均价
	ShortLeverage         decimal.Decimal `json:"short_leverage"`           // 空仓杠杆倍数 逐仓字段
	ShortLiquiPrice       decimal.Decimal `json:"short_liqui_price"`        // 空仓强平价格 逐仓字段
	ShortMaintMarginRatio decimal.Decimal `json:"short_maint_margin_ratio"` // 空仓维持保证金率 逐仓字段
	ShortMargin           decimal.Decimal `json:"short_margin"`             // 空仓保证金
	ShortMarginRatio      decimal.Decimal `json:"short_margin_ratio"`       // 空仓保证金率 逐仓字段
	ShortOpenOutstanding  decimal.Decimal `json:"short_open_outstanding"`   // 空仓开仓冻结张数
	ShortPnl              decimal.Decimal `json:"short_pnl"`                // 空仓收益
	ShortPnlRatio         decimal.Decimal `json:"short_pnl_ratio"`          // 空仓收益率
	ShortQty              decimal.Decimal `json:"short_qty"`                // 空仓数量
	ShortSettledPnl       decimal.Decimal `json:"short_settled_pnl"`        // 空仓已结算收益
	ShortSettlementPrice  decimal.Decimal `json:"short_settlement_price"`   // 结算基准价
	ShortUnrealisedPnl    decimal.Decimal `json:"short_unrealised_pnl"`     // 空仓未实现盈亏
	Timestamp             time.Time       `json:"timestamp"`                // 账户数据更新时间
	UpdatedAt             time.Time       `json:"updated_at"`               // 更新时间
}
