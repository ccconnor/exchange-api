package types

// CrossedAccount 交割合约全仓账户信息
type CrossedAccount struct {
	CanWithdraw       string `json:"can_withdraw"`        // 可划转数量
	Currency          string `json:"currency"`            // 账户余额币种
	Equity            string `json:"equity"`              // 账户权益
	LiquiFeeRate      string `json:"liqui_fee_rate"`      // 强平手续费
	LiquiMode         string `json:"liqui_mode"`          // 强平模式：tier（梯度强平）
	MaintMarginRatio  string `json:"maint_margin_ratio"`  // 维持保证金率
	Margin            string `json:"margin"`              // 保证金（挂单冻结+持仓已用）
	MarginForUnfilled string `json:"margin_for_unfilled"` // 挂单冻结保证金
	MarginFrozen      string `json:"margin_frozen"`       // 持仓已用保证金
	MarginMode        string `json:"margin_mode"`         // 账户类型全仓：crossed
	MarginRatio       string `json:"margin_ratio"`        // 保证金率
	RealizedPnl       string `json:"realized_pnl"`        // 已实现盈亏
	TotalAvailBalance string `json:"total_avail_balance"` // 账户余额
	Underlying        string `json:"underlying"`          // 标的指数，如：BTC-USD，BTC-USDT
	UnrealizedPnl     string `json:"unrealized_pnl"`      // 未实现盈亏
}

// FixedAccount 交割合约逐仓账户信息
type FixedAccount struct {
	Contracts []struct {
		AvailableQty      string `json:"available_qty"`       // 逐仓可用余额
		FixedBalance      string `json:"fixed_balance"`       // 逐仓账户余额
		InstrumentID      string `json:"instrument_id"`       // 合约ID，如BTC-USD-180213，BTC-USDT-191227
		MarginForUnfilled string `json:"margin_for_unfilled"` // 挂单冻结保证金
		MarginFrozen      string `json:"margin_frozen"`       // 持仓已用保证金
		RealizedPnl       string `json:"realized_pnl"`        // 已实现盈亏
		UnrealizedPnl     string `json:"unrealized_pnl"`      // 未实现盈亏
	} `json:"contracts"`
	TotalAvailBalance string `json:"total_avail_balance"` // 账户余额（账户静态权益）
	Equity            string `json:"equity"`              // 账户权益（账户动态权益）
	MarginMode        string `json:"margin_mode"`         // 账户类型逐仓：fixed
	AutoMargin        string `json:"auto_margin"`         // 是否自动追加保证金1: 自动追加已开启0: 自动追加未开启
	LiquiMode         string `json:"liqui_mode"`          // 强平模式：tier（梯度强平）
	CanWithdraw       string `json:"can_withdraw"`        // 可划转数量
	Currency          string `json:"currency"`            // 账户余额币种
}
