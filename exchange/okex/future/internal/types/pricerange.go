package types

import (
	"time"

	"github.com/shopspring/decimal"
)

// TablePriceRange ws推送格式
type TablePriceRange struct {
	Highest      decimal.Decimal `json:"highest"`
	Lowest       decimal.Decimal `json:"lowest"`
	InstrumentID string          `json:"instrument_id"`
	Timestamp    time.Time       `json:"timestamp"`
}
