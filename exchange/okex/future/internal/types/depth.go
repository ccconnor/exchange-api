package types

import (
	"time"

	"github.com/shopspring/decimal"
)

// TableDepth5 交割合约websocket推送5档深度频道
type TableDepth5 struct {
	Asks         [][4]decimal.Decimal `json:"asks"`
	Bids         [][4]decimal.Decimal `json:"bids"`
	InstrumentID string               `json:"instrument_id"`
	Timestamp    time.Time            `json:"timestamp"`
}

// 交割合约websocket推送400档深度频道
