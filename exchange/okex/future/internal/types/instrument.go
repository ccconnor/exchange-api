package types

import (
	"github.com/shopspring/decimal"
)

// Instrument 合约信息
type Instrument struct {
	InstrumentID        string          `json:"instrument_id"`         // 合约ID，如BTC-USD-180213,BTC-USDT-191227
	Underlying          string          `json:"underlying"`            // 标的指数，如：BTC-USD
	BaseCurrency        string          `json:"base_currency"`         // 交易货币，如：BTC-USD中的BTC ,BTC-USDT中的BTC
	QuoteCurrency       string          `json:"quote_currency"`        // 计价货币币种，如：BTC-USD中的USD ,BTC-USDT中的USDT
	SettlementCurrency  string          `json:"settlement_currency"`   // 盈亏结算和保证金币种，如BTC
	ContractVal         decimal.Decimal `json:"contract_val"`          // 合约面值(美元)
	Listing             string          `json:"listing"`               // 上线日期
	Delivery            string          `json:"delivery"`              // 交割日期
	TickSize            decimal.Decimal `json:"tick_size"`             // 下单价格精度
	TradeIncrement      decimal.Decimal `json:"trade_increment"`       // 下单数量精度
	Alias               string          `json:"alias"`                 // 本周 this_week   次周 next_week   季度 quarter   次季度 bi_quarter
	IsInverse           string          `json:"is_inverse"`            // true  or  false  ,是否 币本位保证金合约
	ContractValCurrency string          `json:"contract_val_currency"` // 合约面值计价币种  如  usd，btc，ltc，etc xrp  eos
}
