package types

import (
	"time"

	"github.com/shopspring/decimal"
)

// Order 交割合约订单信息
type Order struct {
	InstrumentID string          `json:"instrument_id"` // 合约ID，如BTC-USD-180213,BTC-USDT-191227
	ClientOid    string          `json:"client_oid"`    // 由您设置的订单ID来识别您的订单
	Size         decimal.Decimal `json:"size"`          // 委托数量
	Timestamp    time.Time       `json:"timestamp"`     // 委托时间
	FilledQty    decimal.Decimal `json:"filled_qty"`    // 成交数量
	Fee          decimal.Decimal `json:"fee"`           // 手续费
	OrderID      string          `json:"order_id"`      // 订单ID
	Price        decimal.Decimal `json:"price"`         // 委托价格
	PriceAvg     decimal.Decimal `json:"price_avg"`     // 成交均价
	Type         decimal.Decimal `json:"type"`          // 订单类型1:开多2:开空3:平多4:平空
	ContractVal  decimal.Decimal `json:"contract_val"`  // 合约面值
	Leverage     decimal.Decimal `json:"leverage"`      // 杠杆倍数，1-100的数值
	OrderType    decimal.Decimal `json:"order_type"`    // 0：普通委托 1：只做Maker（Post only） 2：全部成交或立即取消（FOK） 3：立即成交并取消剩余（IOC）   4：市价委托
	Pnl          decimal.Decimal `json:"pnl"`           // 收益
	State        decimal.Decimal `json:"state"`         // 订单状态-2：失败 -1：撤单成功 0：等待成交 1：部分成交  2：完全成交 3：下单中 4：撤单中
}

// TableOrder websocket交割合约用户交易频道返回参数
type TableOrder struct {
	InstrumentID string          `json:"instrument_id"`  // 合约ID，如BTC-USDT-180213,BTC-USDT-191227
	ClientOid    string          `json:"client_oid"`     // 由用户设置的订单ID
	Size         decimal.Decimal `json:"size"`           // 委托数量
	Timestamp    time.Time       `json:"timestamp"`      // 订单状态变化时间
	FilledQty    decimal.Decimal `json:"filled_qty"`     // 成交数量
	Fee          decimal.Decimal `json:"fee"`            // 手续费
	OrderID      string          `json:"order_id"`       // 订单ID
	Price        decimal.Decimal `json:"price"`          // 委托价格
	ErrorCode    string          `json:"error_code"`     // 错误码
	Pnl          decimal.Decimal `json:"pnl"`            // 收益
	PriceAvg     decimal.Decimal `json:"price_avg"`      // 成交均价
	Type         decimal.Decimal `json:"type"`           // 订单类型1:开多2:开空3:平多4:平空
	ContractVal  decimal.Decimal `json:"contract_val"`   // 合约面值
	Leverage     decimal.Decimal `json:"leverage"`       // 杠杆倍数
	OrderType    decimal.Decimal `json:"order_type"`     // 0：普通委托 1：只做Maker（Post only） 2：全部成交或立即取消（FOK） 3：立即成交并取消剩余（IOC）  4：市价委托
	LastFillPx   decimal.Decimal `json:"last_fill_px"`   // 最新成交价格（如果没有，推0）
	LastFillID   string          `json:"last_fill_id"`   // 最新成交id（如果没有，推0），和trade频道推送的trade_id一致
	LastFillQty  decimal.Decimal `json:"last_fill_qty"`  // 最新成交数量（如果没有，推0）
	LastFillTime time.Time       `json:"last_fill_time"` // 最新成交时间（如果没有，推1970-01-01T00:00:00.000Z）
	State        decimal.Decimal `json:"state"`          // -2:失败-1:撤单成功0:等待成交1:部分成交2:完全成交3:下单中4:撤单中
}
