package websocket

import (
	"gitee.com/ccconnor/exchange-api/exchange/okex/future"
	"log"
	"testing"
	"time"
)

func TestConnect(t *testing.T) {
	endpoint := "wss://real.okex.com:8443/ws/v3?brokerId=181"
	apikey := "27ad80b0-4a1c-40c5-a24d-dc2140990c2a"
	secretkey := "9415A393A5A679E400FB0978168F4AE1"
	password := "123456"
	topics := []string{"futures/order:TBTC-USD-200612"}
	recvMsg := func(message *TableMessage) {
		switch message.Table {
		case ChnlFuturesPriceRange:
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*future.TablePriceRange)))
			for _, v := range message.Data.([]*future.TablePriceRange) {
				log.Printf("%+v", *v)
			}
		case ChnlFuturesDepth5:
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*future.TableDepth5)))
			for _, v := range message.Data.([]*future.TableDepth5) {
				log.Printf("%+v", *v)
			}
		case ChnlFuturesPosition:
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*future.TablePosition)))
			for _, v := range message.Data.([]*future.TablePosition) {
				log.Printf("%+v", *v)
			}
		case ChnlFuturesOrder:
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*future.TableOrder)))
			for _, v := range message.Data.([]*future.TableOrder) {
				log.Printf("%+v", *v)
			}
		}
	}

	w := NewClient(endpoint, apikey, secretkey, password, topics, recvMsg)
	if err := w.Connect(); err != nil {
		t.Error("connect failed", err)
		return
	}
	log.Println("websocket connected")

	time.Sleep(time.Minute * 10)
}
