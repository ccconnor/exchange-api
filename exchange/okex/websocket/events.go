package websocket

const (
	EventTypeLogin       = "login"
	EventTypeSubscribe   = "subscribe"
	EventTypeUnsubscribe = "unsubscribe"
	EventTypeError       = "error"
)

type EventLogin struct {
	Event   string `json:"event"`
	Success bool   `json:"success"`
}

type EventSubscribe struct {
	Event   string `json:"event"`
	Channel string `json:"channel"`
}

type EventUnsubscribe struct {
	Event   string `json:"event"`
	Channel string `json:"channel"`
}

type EventError struct {
	Event     string `json:"event"`
	Message   string `json:"message"`
	ErrorCode int    `json:"errorCode"`
}
