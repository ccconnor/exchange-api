package websocket

type TableMessage struct {
	Action string      `json:"action"`
	Table  string      `json:"table"`
	Data   interface{} `json:"data"`
}
