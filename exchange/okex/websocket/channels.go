package websocket

const (
	ChnlFuturesPriceRange = "futures/price_range" // 限价范围频道
	ChnlFuturesDepth5     = "futures/depth5"      // 深度数据频道，每次返回前5档
	ChnlFuturesPosition   = "futures/position"    // 用户持仓信息频道
	ChnlFuturesOrder      = "futures/order"       // 用户交易数据频道
)
