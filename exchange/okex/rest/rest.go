package rest

import (
	"fmt"

	"gitee.com/ccconnor/exchange-api/exchange/okex/future"
	"gitee.com/ccconnor/exchange-api/exchange/okex/internal"
)

type Client struct {
	Futures *future.Client
}

func NewClient(endpoint, apiKey, secretKey, password string) *Client {
	c := internal.NewClient(endpoint, apiKey, secretKey, password)
	if c == nil {
		fmt.Println("new client failed")
		return nil
	}
	return &Client{Futures: &future.Client{HTTPClient: c}}
}
