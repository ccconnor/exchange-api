package futuretest

import (
	"fmt"
	"gitee.com/ccconnor/exchange-api/exchange/okex/rest"
	"testing"
)

func TestGetAllPositions(t *testing.T) {
	apikey := "27ad80b0-4a1c-40c5-a24d-dc2140990c2a"
	secretkey := "9415A393A5A679E400FB0978168F4AE1"
	password := "123456"
	endpoint := "https://testnet.okex.me"
	restHandler := rest.NewClient(endpoint, apikey, secretkey, password)

	res, err := restHandler.Futures.GetAllPositions()
	if err != nil {
		t.Error(err)
		return
	}
	if !res.Result {
		t.Error("get position failed")
		return
	}
	fmt.Printf("%+v\n", res)
}

func TestGetPosition(t *testing.T) {
	apikey := "27ad80b0-4a1c-40c5-a24d-dc2140990c2a"
	secretkey := "9415A393A5A679E400FB0978168F4AE1"
	password := "123456"
	endpoint := "https://testnet.okex.me"
	restHandler := rest.NewClient(endpoint, apikey, secretkey, password)

	res, err := restHandler.Futures.GetPosition("TBTC-USD-200619")
	if err != nil {
		t.Error(err)
		return
	}
	if !res.Result {
		t.Error("get position failed")
		return
	}
	fmt.Printf("%+v\n", res)
}
