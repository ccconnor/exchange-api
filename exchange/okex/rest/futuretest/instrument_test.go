package futuretest

import (
	"fmt"
	"gitee.com/ccconnor/exchange-api/exchange/okex/rest"
	"testing"
)

func TestGetInstruments(t *testing.T) {
	endpoint := "https://www.okex.com"
	restHandler := rest.NewClient(endpoint, "", "", "")

	instruments, err := restHandler.Futures.GetInstruments()
	if err != nil {
		t.Error(err)
		return
	}
	format := "%-20v%-16v%-16v%-16v\n"
	fmt.Printf(format, "InstrumentID", "Underlying", "BaseCurrency", "QuoteCurrency")
	for _, v := range instruments {
		fmt.Printf(format, v.InstrumentID, v.Underlying, v.BaseCurrency, v.QuoteCurrency)
	}
	fmt.Printf("Got %d instrument(s)\n", len(instruments))
}
