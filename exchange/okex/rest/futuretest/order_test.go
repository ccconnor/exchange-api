package futuretest

import (
	"fmt"
	"gitee.com/ccconnor/exchange-api/exchange/okex/future"
	"gitee.com/ccconnor/exchange-api/exchange/okex/rest"
	"gitee.com/ccconnor/exchange-api/utils"
	"testing"
)

func TestNewOrder(t *testing.T) {
	apikey := "27ad80b0-4a1c-40c5-a24d-dc2140990c2a"
	secretkey := "9415A393A5A679E400FB0978168F4AE1"
	password := "123456"
	endpoint := "https://testnet.okex.me"
	restHandler := rest.NewClient(endpoint, apikey, secretkey, password)

	params := &future.NewOrderParam{
		InstrumentID: "TBTC-USD-200619",
		Type:         1,
		Price:        utils.NewFloat64(8000),
		Size:         10,
	}

	order, err := restHandler.Futures.NewOrder(params)
	if err != nil {
		t.Error(err)
		return
	}
	if !order.Result {
		t.Error(order.ErrorCode, order.ErrorMessage)
		return
	}
	fmt.Printf("%+v\n", order)
}

func TestGetOrder(t *testing.T) {
	apikey := "27ad80b0-4a1c-40c5-a24d-dc2140990c2a"
	secretkey := "9415A393A5A679E400FB0978168F4AE1"
	password := "123456"
	endpoint := "https://testnet.okex.me"
	restHandler := rest.NewClient(endpoint, apikey, secretkey, password)

	order, err := restHandler.Futures.GetOrder("TBTC-USD-200619", "5071771992056832")
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Printf("%+v\n", order)
}

func TestCancelOrder(t *testing.T) {
	apikey := "27ad80b0-4a1c-40c5-a24d-dc2140990c2a"
	secretkey := "9415A393A5A679E400FB0978168F4AE1"
	password := "123456"
	endpoint := "https://testnet.okex.me"
	restHandler := rest.NewClient(endpoint, apikey, secretkey, password)

	resp, err := restHandler.Futures.CancelOrder("TBTC-USD-200619", "5071771992056832")
	if err != nil {
		t.Error(err)
		return
	}
	if !resp.Result {
		t.Error(resp.ErrorCode, resp.ErrorMessage)
		return
	}
	fmt.Printf("%+v\n", resp)
}

func TestGetOrders(t *testing.T) {
	apikey := "27ad80b0-4a1c-40c5-a24d-dc2140990c2a"
	secretkey := "9415A393A5A679E400FB0978168F4AE1"
	password := "123456"
	endpoint := "https://testnet.okex.com"
	restHandler := rest.NewClient(endpoint, apikey, secretkey, password)

	param := future.GetOrdersParam{InstrumentID: "TBTC-USD-200619", State: future.OrderStateUnfinished}
	resp, err := restHandler.Futures.GetOrders(param)
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Printf("%+v\n", resp)
}
