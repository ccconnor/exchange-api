package futuretest

import (
	"fmt"
	"gitee.com/ccconnor/exchange-api/exchange/okex/rest"
	"testing"
)

func TestGetAccount(t *testing.T) {
	apikey := "27ad80b0-4a1c-40c5-a24d-dc2140990c2a"
	secretkey := "9415A393A5A679E400FB0978168F4AE1"
	password := "123456"
	endpoint := "https://testnet.okex.me"
	restHandler := rest.NewClient(endpoint, apikey, secretkey, password)

	resp1, err := restHandler.Futures.GetCrossedAccount("TBTC-USD")
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Printf("%+v\n", resp1)

	resp2, err := restHandler.Futures.GetFixedAccount("TBTC-USD")
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Printf("%+v\n", resp2)
}
