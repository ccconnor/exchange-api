package futuretest

import (
	"fmt"
	"gitee.com/ccconnor/exchange-api/exchange/okex/future"
	"gitee.com/ccconnor/exchange-api/exchange/okex/rest"
	"gitee.com/ccconnor/exchange-api/utils"
	"testing"
)

func TestGetDepth(t *testing.T) {
	apikey := "27ad80b0-4a1c-40c5-a24d-dc2140990c2a"
	secretkey := "9415A393A5A679E400FB0978168F4AE1"
	password := "123456"
	endpoint := "https://testnet.okex.me"
	restHandler := rest.NewClient(endpoint, apikey, secretkey, password)

	param := future.GetDepthParam{InstrumentID: "TBTC-USD-200619", Size: utils.NewInt(10)}
	resp, err := restHandler.Futures.GetDepth(param)
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Printf("%+v\n", resp)
}
