package rest

import (
	"fmt"
	"reflect"

	"gitee.com/ccconnor/exchange-api/exchange/bitmex/internal"
	"github.com/mitchellh/mapstructure"
)

type Client struct {
	httpClient *internal.Client
}

func NewClient(endpoint, apiKey, secretKey string) *Client {
	c := internal.NewClient(endpoint, apiKey, secretKey)
	if c == nil {
		fmt.Println("new client failed")
		return nil
	}
	return &Client{httpClient: c}
}

func (c Client) request(method, path string, param, resp interface{}, needSign bool) error {
	var paramsMap map[string]interface{}
	if param != nil && !reflect.ValueOf(param).IsNil() {
		err := mapstructure.Decode(param, &paramsMap)
		if err != nil {
			return err
		}
	}

	return c.httpClient.RequestMap(method, path, paramsMap, &resp, needSign)
}
