package rest

import (
	"fmt"
	"testing"
)

func TestGetOrderBook(t *testing.T) {
	endpoint := "https://testnet.bitmex.com"
	apiKey := ""
	secretKey := ""
	bitmexRestCli := NewClient(endpoint, apiKey, secretKey)

	orderBook, err := bitmexRestCli.GetOrderBook("XBTUSD", 10)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("Got %d orderBook(s)\n", len(orderBook))
	for _, v := range orderBook {
		fmt.Printf("%+v\n", *v)
	}
}
