package rest

import (
	"fmt"
	"gitee.com/ccconnor/exchange-api/utils"
	"testing"
)

func TestGetOrder(t *testing.T) {
	endpoint := "https://testnet.bitmex.com"
	apiKey := "cihb_hjaTRycwJ9ML4UEIGgR"
	secretKey := "ZTbKCf47aftV6PvoX7QFvU1EX6wsdzsAc7L-_K-JjHrIrxwv"
	bitmexRestCli := NewClient(endpoint, apiKey, secretKey)
	param := &GetOrderParam{Filter: map[string]interface{}{"workingIndicator": true}}
	orderList, err := bitmexRestCli.GetOrder(param)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("Got %d order(s)\n", len(orderList))
	for _, v := range orderList {
		fmt.Printf("%+v\n", *v)
	}
}

func TestNewOrder(t *testing.T) {
	endpoint := "https://testnet.bitmex.com"
	apiKey := "cihb_hjaTRycwJ9ML4UEIGgR"
	secretKey := "ZTbKCf47aftV6PvoX7QFvU1EX6wsdzsAc7L-_K-JjHrIrxwv"
	bitmexRestCli := NewClient(endpoint, apiKey, secretKey)

	params := &NewOrderParam{
		Symbol:   "XBTUSD",
		OrderQty: utils.NewInt64(1),
		Price:    utils.NewFloat64(4000),
		OrdType:  utils.NewString("Limit"),
	}

	order, err := bitmexRestCli.NewOrder(params)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("%+v\n", *order)
}

func TestCancelSingleOrder(t *testing.T) {
	endpoint := "https://testnet.bitmex.com"
	apiKey := "cihb_hjaTRycwJ9ML4UEIGgR"
	secretKey := "ZTbKCf47aftV6PvoX7QFvU1EX6wsdzsAc7L-_K-JjHrIrxwv"
	bitmexRestCli := NewClient(endpoint, apiKey, secretKey)

	// 下单
	params := &NewOrderParam{
		Symbol:   "XBTUSD",
		OrderQty: utils.NewInt64(1),
		Price:    utils.NewFloat64(4000),
		OrdType:  utils.NewString("Limit"),
	}
	order, err := bitmexRestCli.NewOrder(params)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("new order: %+v\n", *order)

	// 单独撤单
	cancelParam := &CancelSingleOrderParam{OrderID: utils.NewString(order.OrderID)}
	order, err = bitmexRestCli.CancelSingleOrder(cancelParam)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("canceled order%+v\n", *order)
}

func TestCancelOrder(t *testing.T) {
	endpoint := "https://testnet.bitmex.com"
	apiKey := "cihb_hjaTRycwJ9ML4UEIGgR"
	secretKey := "ZTbKCf47aftV6PvoX7QFvU1EX6wsdzsAc7L-_K-JjHrIrxwv"
	bitmexRestCli := NewClient(endpoint, apiKey, secretKey)

	// 下单
	params := &NewOrderParam{
		Symbol:   "XBTUSD",
		OrderQty: utils.NewInt64(1),
		Price:    utils.NewFloat64(4000),
		OrdType:  utils.NewString("Limit"),
	}
	order, err := bitmexRestCli.NewOrder(params)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("new order: %+v\n", *order)

	// 批量撤单
	cancelParam := &CancelOrderParam{OrderID: []string{order.OrderID}}
	orderList, err := bitmexRestCli.CancelOrder(cancelParam)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("canceled %d order\n", len(orderList))
	for _, v := range orderList {
		fmt.Printf("%+v\n", *v)
	}
}

func TestCancelAllOrder(t *testing.T) {
	endpoint := "https://testnet.bitmex.com"
	apiKey := "cihb_hjaTRycwJ9ML4UEIGgR"
	secretKey := "ZTbKCf47aftV6PvoX7QFvU1EX6wsdzsAc7L-_K-JjHrIrxwv"
	bitmexRestCli := NewClient(endpoint, apiKey, secretKey)

	// 下单
	params := &NewOrderParam{
		Symbol:   "XBTUSD",
		OrderQty: utils.NewInt64(1),
		Price:    utils.NewFloat64(4000),
		OrdType:  utils.NewString("Limit"),
	}
	order, err := bitmexRestCli.NewOrder(params)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("new order: %+v\n", *order)

	// 撤单
	orderList, err := bitmexRestCli.CancelAllOrder(nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("canceled %d order\n", len(orderList))
	for _, v := range orderList {
		fmt.Printf("%+v\n", *v)
	}
}
