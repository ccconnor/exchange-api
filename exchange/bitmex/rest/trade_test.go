package rest

import (
	"fmt"
	"gitee.com/ccconnor/exchange-api/utils"
	"testing"
	"time"
)

func TestGetBucketedTrade(t *testing.T) {
	endpoint := "https://testnet.bitmex.com"
	apiKey := ""
	secretKey := ""
	bitmexRestCli := NewClient(endpoint, apiKey, secretKey)

	st := time.Now().Add(-time.Hour).UTC()
	param := &GetBucketedTradeParam{
		BinSize:   "1m",
		StartTime: utils.NewString(st.Format(time.RFC3339)),
		//EndTime:   utils.NewString(et.Format(time.RFC3339)),
		//Start:     utils.NewInt32(1),
		Count:  utils.NewInt32(1),
		Filter: map[string]interface{}{"symbol": "XBTUSD"},
	}
	trades, err := bitmexRestCli.GetBucketedTrade(param)
	if err != nil {
		t.Error(err)
		return
	}

	for _, v := range trades {
		fmt.Printf("%+v\n", *v)
	}
	fmt.Printf("Got %d trade(s)\n", len(trades))
}
