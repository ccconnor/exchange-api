package rest

import (
	"time"

	"gitee.com/ccconnor/exchange-api/exchange/bitmex/types"
)

type GetOrderParam struct {
	Symbol    *string                `mapstructure:"symbol,omitempty"`
	Filter    map[string]interface{} `mapstructure:"filter,omitempty"`
	Columns   []string               `mapstructure:"columns,omitempty"`
	Count     *int                   `mapstructure:"count,omitempty"`
	Start     *int                   `mapstructure:"start,omitempty"`
	Reverse   *bool                  `mapstructure:"reverse,omitempty"`
	StartTime *time.Time             `mapstructure:"startTime,omitempty"`
	EndTime   *time.Time             `mapstructure:"endTime,omitempty"`
}

func (c Client) GetOrder(params *GetOrderParam) (resp []*types.Order, err error) {
	path := "/api/v1/order" //nolint:goconst // make lint happy
	err = c.httpClient.RequestStruct("GET", path, params, &resp, true)
	return
}

type NewOrderParam struct {
	Symbol         string   `mapstructure:"symbol"`
	Side           *string  `mapstructure:"side,omitempty"`
	OrderQty       *int64   `mapstructure:"orderQty,omitempty"`
	Price          *float64 `mapstructure:"price,omitempty"`
	DisplayQty     *int64   `mapstructure:"displayQty,omitempty"`
	StopPx         *float64 `mapstructure:"stopPx,omitempty"`
	ClOrdID        *string  `mapstructure:"clOrdID,omitempty"`
	PegOffsetValue *float64 `mapstructure:"pegOffsetValue,omitempty"`
	PegPriceType   *string  `mapstructure:"pegPriceType,omitempty"`
	OrdType        *string  `mapstructure:"ordType,omitempty"`
	TimeInForce    *string  `mapstructure:"timeInForce,omitempty"`
	ExecInst       *string  `mapstructure:"execInst,omitempty"`
	Text           *string  `mapstructure:"text,omitempty"`
}

func (c Client) NewOrder(params *NewOrderParam) (resp *types.Order, err error) {
	path := "/api/v1/order"
	err = c.httpClient.RequestStruct("POST", path, params, &resp, true)
	return
}

type CancelSingleOrderParam struct {
	OrderID *string `mapstructure:"orderID,omitempty"`
	ClOrdID *string `mapstructure:"clOrdID,omitempty"`
	Text    *string `mapstructure:"text,omitempty"`
}

func (c Client) CancelSingleOrder(params *CancelSingleOrderParam) (*types.Order, error) {
	path := "/api/v1/order"
	var orderList []*types.Order
	err := c.httpClient.RequestStruct("DELETE", path, params, &orderList, true)
	if err != nil {
		return nil, err
	}

	return orderList[0], nil
}

type CancelOrderParam struct {
	OrderID []string `mapstructure:"orderID,omitempty"`
	ClOrdID []string `mapstructure:"clOrdID,omitempty"`
	Text    *string  `mapstructure:"text,omitempty"`
}

type CancelOrderResp struct {
	types.Order
	Error string `json:"error"`
}

func (c Client) CancelOrder(params *CancelOrderParam) (resp []*CancelOrderResp, err error) {
	path := "/api/v1/order"
	err = c.httpClient.RequestStruct("DELETE", path, params, &resp, true)
	return
}

type CancelAllOrderParam struct {
	Symbol *string                `mapstructure:"symbol,omitempty"`
	Filter map[string]interface{} `mapstructure:"filter,omitempty"`
	Text   *string                `mapstructure:"text,omitempty"`
}

func (c Client) CancelAllOrder(params *CancelAllOrderParam) (resp []*types.Order, err error) {
	path := "/api/v1/order/all"
	err = c.httpClient.RequestStruct("DELETE", path, params, &resp, true)
	return
}
