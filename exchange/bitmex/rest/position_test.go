package rest

import (
	"fmt"
	"testing"
)

func TestGetPosition(t *testing.T) {
	endpoint := "https://testnet.bitmex.com"
	apiKey := "cihb_hjaTRycwJ9ML4UEIGgR"
	secretKey := "ZTbKCf47aftV6PvoX7QFvU1EX6wsdzsAc7L-_K-JjHrIrxwv"
	bitmexRestCli := NewClient(endpoint, apiKey, secretKey)
	filter := map[string]interface{}{
		"symbol": "XBTUSD",
	}
	params := &GetPositionParam{Filter: filter}
	positionList, err := bitmexRestCli.GetPosition(params)
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Printf("Got %d position(s)\n", len(positionList))
}

func TestUpdatePositionLeverage(t *testing.T) {
	endpoint := "https://testnet.bitmex.com"
	apiKey := "cihb_hjaTRycwJ9ML4UEIGgR"
	secretKey := "ZTbKCf47aftV6PvoX7QFvU1EX6wsdzsAc7L-_K-JjHrIrxwv"
	bitmexRestCli := NewClient(endpoint, apiKey, secretKey)
	position, err := bitmexRestCli.UpdatePositionLeverage("XBTUSD", 1.0)
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Println(*position)
}
