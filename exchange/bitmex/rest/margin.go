package rest

import "gitee.com/ccconnor/exchange-api/exchange/bitmex/types"

// 当前bitmex只有XBt
func (c Client) GetUserMargin() (resp *types.Margin, err error) {
	path := "/api/v1/user/margin"
	err = c.httpClient.RequestMap("GET", path, nil, &resp, true)
	return
}
