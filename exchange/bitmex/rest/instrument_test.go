package rest

import (
	"fmt"
	"gitee.com/ccconnor/exchange-api/utils"
	"testing"
)

func TestGetInstrument(t *testing.T) {
	endpoint := "https://testnet.bitmex.com"
	apiKey := ""
	secretKey := ""
	bitmexRestCli := NewClient(endpoint, apiKey, secretKey)

	instruments, err := bitmexRestCli.GetInstrument(&GetInstrumentParam{Count: utils.NewInt(50)})
	if err != nil {
		t.Error(err)
		return
	}
	format := "%-16v%-16v%-16v%-16v\n"
	fmt.Printf(format, "Symbol", "RootSymbol", "Underlying", "Expiry")
	for _, v := range instruments {
		fmt.Printf(format, v.Symbol, v.RootSymbol, v.Underlying, v.Expiry)
	}
	fmt.Printf("Got %d instrument(s)\n", len(instruments))
}

func TestGetActiveInstrument(t *testing.T) {
	endpoint := "https://testnet.bitmex.com"
	bitmexRestCli := NewClient(endpoint, "", "")

	instruments, err := bitmexRestCli.GetActiveInstrument()
	if err != nil {
		t.Error(err)
		return
	}
	format := "%-16v%-16v%-16v%-16v\n"
	fmt.Printf(format, "Symbol", "RootSymbol", "Underlying", "Expiry")
	for _, v := range instruments {
		fmt.Printf(format, v.Symbol, v.RootSymbol, v.Underlying, v.Expiry)
	}
	fmt.Printf("Got %d instrument(s)\n", len(instruments))
}
