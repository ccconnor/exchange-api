package rest

import (
	"gitee.com/ccconnor/exchange-api/exchange/bitmex/types"
)

func (c Client) GetOrderBook(symbol string, depth int) (resp []*types.OrderBook, err error) {
	path := "/api/v1/orderBook/L2"
	params := map[string]interface{}{
		"symbol": symbol,
		"depth":  depth,
	}

	err = c.httpClient.RequestMap("GET", path, params, &resp, false)
	return
}
