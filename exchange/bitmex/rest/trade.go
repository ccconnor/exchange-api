package rest

import (
	"gitee.com/ccconnor/exchange-api/exchange/bitmex/types"
)

type GetBucketedTradeParam struct {
	/*BinSize
	  Time interval to bucket by. Available options: [1m,5m,1h,1d].
	*/
	BinSize string `mapstructure:"binSize"`
	/*Columns
	  Array of column names to fetch. If omitted, will return all columns.
	  Note that this method will always return item keys, even when not specified, so you may receive more columns that you expect.
	*/
	Columns *string `mapstructure:"columns,omitempty"`
	/*Count
	  Number of results to fetch.
	*/
	Count *int32 `mapstructure:"count,omitempty"`
	/*EndTime
	  Ending date filter for results.
	*/
	EndTime *string `mapstructure:"endTime,omitempty"`
	/*Filter
	  Generic table filter. Send JSON key/value pairs, such as `{"key": "value"}`. You can key on individual fields, and do more advanced querying on timestamps. See the [Timestamp Docs](https://www.bitmex.com/app/restAPI#Timestamp-Filters) for more details.
	*/
	Filter map[string]interface{} `mapstructure:"filter,omitempty"`
	/*Partial
	  If true, will send in-progress (incomplete) bins for the current time period.
	*/
	Partial *bool `mapstructure:"partial,omitempty"`
	/*Reverse
	  If true, will sort results newest first.
	*/
	Reverse *bool `mapstructure:"reverse,omitempty"`
	/*Start
	  Starting point for results.
	*/
	Start *int32 `mapstructure:"start,omitempty"`
	/*StartTime
	  Starting date filter for results. ISO time format
	*/
	StartTime *string `mapstructure:"startTime,omitempty"`
	/*Symbol
	  Instrument symbol. Send a bare series (e.g. XBT) to get data for the nearest expiring contract in that series.
	  You can also send a timeframe, e.g. `XBT:quarterly`. Timeframes are `nearest`, `daily`, `weekly`, `monthly`, `quarterly`, `biquarterly`, and `perpetual`.
	*/
	Symbol *string `mapstructure:"symbol,omitempty"`
}

func (c Client) GetBucketedTrade(params *GetBucketedTradeParam) (resp []*types.TradeBin, err error) {
	path := "/api/v1/trade/bucketed"
	err = c.httpClient.RequestStruct("GET", path, params, &resp, false)
	return
}
