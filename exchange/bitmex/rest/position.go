package rest

import (
	"gitee.com/ccconnor/exchange-api/exchange/bitmex/types"
)

type GetPositionParam struct {
	Filter  map[string]interface{} `mapstructure:"filter,omitempty"`
	Columns []string               `mapstructure:"columns,omitempty"`
	Count   *int                   `mapstructure:"count,omitempty"`
}

func (c Client) GetPosition(params *GetPositionParam) (resp []*types.Position, err error) {
	path := "/api/v1/position"
	err = c.httpClient.RequestStruct("GET", path, params, &resp, true)
	return
}

func (c Client) UpdatePositionLeverage(symbol string, leverage float64) (resp *types.Position, err error) {
	path := "/api/v1/position/leverage"
	params := map[string]interface{}{
		"symbol":   symbol,
		"leverage": leverage,
	}
	err = c.httpClient.RequestMap("POST", path, params, &resp, true)
	return
}
