package rest

import (
	"time"

	"gitee.com/ccconnor/exchange-api/exchange/bitmex/types"
)

type GetInstrumentParam struct {
	Symbol    *string                `mapstructure:"symbol,omitempty"`
	Filter    map[string]interface{} `mapstructure:"filter,omitempty"`
	Columns   []string               `mapstructure:"columns,omitempty"`
	Count     *int                   `mapstructure:"count,omitempty"`
	Start     *int                   `mapstructure:"start,omitempty"`
	Reverse   *bool                  `mapstructure:"reverse,omitempty"`
	StartTime *time.Time             `mapstructure:"startTime,omitempty"`
	EndTime   *time.Time             `mapstructure:"endTime,omitempty"`
}

func (c Client) GetInstrument(params *GetInstrumentParam) (resp []*types.Instrument, err error) {
	path := "/api/v1/instrument"
	err = c.httpClient.RequestStruct("GET", path, params, &resp, false)
	return
}

func (c Client) GetActiveInstrument() (resp []*types.Instrument, err error) {
	path := "/api/v1/instrument/active"
	err = c.httpClient.RequestMap("GET", path, nil, &resp, false)
	return
}
