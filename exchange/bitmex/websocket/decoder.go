package websocket

import (
	"encoding/json"

	"gitee.com/ccconnor/exchange-api/exchange/bitmex/types"
)

type tableDecoder func(table []byte) interface{}

var tableDecoderMap = map[string]tableDecoder{
	types.TopicTrade:         tradeDecoder,
	types.TopicOrderBookL2:   orderBookDecoder,
	types.TopicPosition:      positionDecoder,
	types.TopicExecution:     execDecoder,
	types.TopicOrderBookL225: orderBookDecoder,
	types.TopicOrder:         orderDecoder,
	types.TopicTradeBin1m:    tradeBinDecoder,
	types.TopicMargin:        marginDecoder,
}

func tradeDecoder(table []byte) interface{} {
	var list []*types.Trade
	_ = json.Unmarshal(table, &list)
	return list
}

func orderBookDecoder(table []byte) interface{} {
	var list []*types.OrderBook
	_ = json.Unmarshal(table, &list)
	return list
}

func positionDecoder(table []byte) interface{} {
	var list []*types.Position
	_ = json.Unmarshal(table, &list)
	return list
}

func execDecoder(table []byte) interface{} {
	var list []*types.Execution
	_ = json.Unmarshal(table, &list)
	return list
}

func orderDecoder(table []byte) interface{} {
	var list []*types.Order
	_ = json.Unmarshal(table, &list)
	return list
}

func tradeBinDecoder(table []byte) interface{} {
	var list []*types.TradeBin
	_ = json.Unmarshal(table, &list)
	return list
}

func marginDecoder(table []byte) interface{} {
	var list []*types.Margin
	_ = json.Unmarshal(table, &list)
	return list
}
