package websocket

import (
	"encoding/json"
	"fmt"
	"gitee.com/ccconnor/exchange-api/exchange/bitmex/types"
	"github.com/gorilla/websocket"
	"log"
	"testing"
	"time"
)

const (
	testWsEndpoint = "wss://testnet.bitmex.com/realtime"
	testApiKey     = "cihb_hjaTRycwJ9ML4UEIGgR"
)

func TestSubscribe(t *testing.T) {
	endpoint := testWsEndpoint
	apiKey := testApiKey
	secretKey := "ZTbKCf47aftV6PvoX7QFvU1EX6wsdzsAc7L-_K-JjHrIrxwv"
	topics := []string{"trade:XBTUSD"}
	msgHandler := func(message *types.Message) {
		switch message.Table {
		case "trade":
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.Trade)))
		case "orderBookL2":
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.OrderBook)))
		}
	}

	// 1. 连接并订阅
	b := NewClient(endpoint, apiKey, secretKey, topics, msgHandler, false)
	err := b.Connect()
	if err != nil {
		t.Error(err)
	}
	log.Println("connected to websocket")
	time.Sleep(10 * time.Second)

	b.Close()

	// 2. 重新连接并订阅
	err = b.Connect()
	if err != nil {
		t.Error(err)
		return
	}
	time.Sleep(10 * time.Second)

	b.Close()

	// 3. 仅连接
	b.topics = make(map[string]struct{})
	err = b.Connect()
	if err != nil {
		t.Error(err)
		return
	}
	time.Sleep(2 * time.Second)

	// 4. 订阅一个主题
	err = b.Subscribe(topics)
	if err != nil {
		t.Error(err)
		return
	}
	time.Sleep(10 * time.Second)

	// 5. 新增一个订阅
	err = b.Subscribe([]string{"orderBookL2:XBTUSD"})
	if err != nil {
		t.Error(err)
		return
	}
	time.Sleep(10 * time.Second)

	// 6. 订阅账户相关主题
	err = b.Subscribe([]string{"position:XBTUSD"})
	if err != nil {
		t.Error(err)
		return
	}
	time.Sleep(10 * time.Second)

	b.Close()
}

func TestStructMessages(t *testing.T) {
	endpoint := testWsEndpoint
	apiKey := testApiKey
	secretKey := "ZTbKCf47aftV6PvoX7QFvU1EX6wsdzsAc7L-_K-JjHrIrxwv"
	topics := []string{types.TopicMargin}
	msgHandler := func(message *types.Message) {
		switch message.Table {
		case types.TopicTrade:
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.Trade)))
		case types.TopicOrderBookL2:
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.OrderBook)))
		case types.TopicPosition:
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.Position)))
		case types.TopicExecution:
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.Execution)))
		case types.TopicOrderBookL225:
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.OrderBook)))
		case types.TopicOrder:
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.Order)))
		case types.TopicTradeBin1m:
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.TradeBin)))
		case types.TopicMargin:
			log.Printf("%s %s %d", message.Action, message.Table, len(message.Data.([]*types.Margin)))
		}
	}

	b := NewClient(endpoint, apiKey, secretKey, topics, msgHandler, false)
	if err := b.Connect(); err != nil {
		t.Error(err)
	}
	log.Println("connected to websocket")

	time.Sleep(1 * time.Minute)

	b.Close()
}

func TestJsonMessages(t *testing.T) {
	endpoint := testWsEndpoint
	apiKey := testApiKey
	secretKey := "ZTbKCf47aftV6PvoX7QFvU1EX6wsdzsAc7L-_K-JjHrIrxwv"
	topics := []string{"orderBookL2_25:XBTUSD"}
	msgHandler := func(message *types.Message) {
		if message.Table == "orderBookL2_25" {
			var orderBookList []map[string]interface{}
			_ = json.Unmarshal(message.Data.([]byte), &orderBookList)
			fmt.Printf("%s %s %d\n", message.Action, message.Table, len(orderBookList))
			for _, v := range orderBookList {
				jsonV, _ := json.Marshal(v)
				var orderBook types.OrderBook
				_ = json.Unmarshal(jsonV, &orderBook)
				fmt.Printf("%+v\n", orderBook)
			}
		}
	}

	b := NewClient(endpoint, apiKey, secretKey, topics, msgHandler, true)
	if err := b.Connect(); err != nil {
		t.Error(err)
	}
	log.Println("connected to websocket")

	time.Sleep(10 * time.Second)

	b.Close()
}

func TestPingPong(t *testing.T) {
	endpoint := testWsEndpoint
	apiKey := ""
	secretKey := ""

	b := NewClient(endpoint, apiKey, secretKey, nil, nil, false)
	err := b.Connect()
	if err != nil {
		t.Error(err)
	}
	log.Println("connected to websocket")

	done := make(chan struct{})
	ticker := time.NewTicker(time.Second * 2)
	defer ticker.Stop()

	count := 0
	for {
		select {
		case <-done:
			return
		case <-ticker.C:
			log.Println("write: ping")
			err := b.conn.WriteMessage(websocket.TextMessage, []byte("ping"))
			if err != nil {
				log.Println("write:", err)
				return
			}
			if count > 10 {
				return
			}
			count++
		}
	}
}
