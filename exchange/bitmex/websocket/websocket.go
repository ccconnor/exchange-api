package websocket

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	"gitee.com/ccconnor/exchange-api/exchange/bitmex/internal"
	"gitee.com/ccconnor/exchange-api/exchange/bitmex/types"
	"github.com/gorilla/websocket"
)

type Client struct {
	endpoint      *url.URL
	apiKey        string
	secretKey     string
	conn          *websocket.Conn
	connected     bool
	closed        bool
	handler       MessageHandler
	topics        map[string]struct{}
	writeMutex    sync.Mutex
	jsonData      bool
	helloTimer    *time.Timer
	deadTimer     *time.Timer
	helloInterval time.Duration
	deadInterval  time.Duration
	wg            sync.WaitGroup
}

type MessageHandler func(message *types.Message)

func NewClient(endpoint, apiKey, secretKey string, topics []string, handler MessageHandler, jsonData bool) *Client {
	u, err := url.Parse(endpoint)
	if err != nil {
		fmt.Println("invalid endpoint")
		return nil
	}

	b := &Client{
		endpoint:      u,
		apiKey:        apiKey,
		secretKey:     secretKey,
		topics:        make(map[string]struct{}),
		handler:       handler,
		jsonData:      jsonData,
		closed:        true,
		helloInterval: 5 * time.Second,
		deadInterval:  10 * time.Second,
	}
	for _, v := range topics {
		b.topics[v] = struct{}{}
	}
	if len(b.topics) > 0 {
		args := KeysString(b.topics)
		b.endpoint.RawQuery = "subscribe=" + args
	}
	return b
}

func (c *Client) Subscribe(topics []string) error {
	if !c.connected {
		log.Println("not connected")
		return errors.New("not connected")
	}

	var args []string
	for _, v := range topics {
		if _, exist := c.topics[v]; !exist {
			c.topics[v] = struct{}{}
			args = append(args, v)
		}
	}
	if len(c.topics) > 0 {
		args := KeysString(c.topics)
		c.endpoint.RawQuery = "subscribe=" + args
	}

	return c.sendCommand("subscribe", args)
}

func (c *Client) Unsubscribe() {

}

func (c *Client) Close() {
	c.close()
	c.closed = true
}

func (c *Client) Connected() bool {
	return c.connected
}

func (c *Client) close() {
	if c.closed {
		return
	}

	if c.helloTimer != nil {
		c.helloTimer.Stop()
	}
	if c.deadTimer != nil {
		c.deadTimer.Stop()
	}
	if c.conn != nil {
		_ = c.conn.Close()
	}
	c.connected = false
	c.wg.Wait()
}

func (c *Client) waitMessage() {
	c.wg.Add(1)
	defer c.wg.Done()
	for {
		_, messageJSON, err := c.conn.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			break
		}

		c.keepAlive()

		var messageMap map[string]interface{}
		_ = json.Unmarshal(messageJSON, &messageMap)
		if subscribe, found := messageMap["subscribe"]; found {
			log.Printf("Subscribed to %s\n", subscribe)
			continue
		}

		action, found := messageMap["action"]
		if !found {
			continue
		}

		if action != "partial" && action != "insert" && action != "update" && action != "delete" {
			log.Println("Unknown action: " + action.(string))
		}

		c.handleMessage(messageMap)
	}

	log.Println("exit websocket read loop")
}

func (c *Client) handleMessage(messageMap map[string]interface{}) {
	if c.handler == nil {
		return
	}

	action := messageMap["action"].(string)
	table := messageMap["table"].(string)
	dataJSON, _ := json.Marshal(messageMap["data"])
	msgStruct := &types.Message{
		Action: action,
		Table:  table,
	}

	if c.jsonData {
		msgStruct.Data = dataJSON
		c.handler(msgStruct)
		return
	}

	if decoder, found := tableDecoderMap[table]; found {
		msgStruct.Data = decoder(dataJSON)
		c.handler(msgStruct)
		return
	}
}

func (c *Client) sendCommand(command string, args []string) error {
	message := map[string]interface{}{
		"op":   command,
		"args": args,
	}

	c.writeMutex.Lock()
	defer c.writeMutex.Unlock()
	err := c.conn.WriteJSON(message)
	return err
}

func (c *Client) sendMessage(msgType int, data []byte) error {
	c.writeMutex.Lock()
	defer c.writeMutex.Unlock()
	return c.conn.WriteMessage(msgType, data)
}

func (c *Client) Connect() error {
	if c.connected {
		return nil
	}

	header := internal.MakeAuthHeader(c.apiKey, c.secretKey, "GET", c.endpoint.Path, nil)

	dialer := &websocket.Dialer{
		Proxy:            http.ProxyFromEnvironment,
		HandshakeTimeout: 20 * time.Second,
	}
	conn, resp, err := dialer.Dial(c.endpoint.String(), header)
	if err != nil {
		switch {
		case resp == nil:
			log.Println("dial:", err)
		case resp.Body == nil:
			log.Printf("dial:%v, status:%v", err, resp.StatusCode)
		default:
			defer resp.Body.Close()
			respBody, _ := io.ReadAll(resp.Body)
			log.Printf("dial:%v, status:%v, info:%v", err, resp.StatusCode, string(respBody))
		}
		return err
	}
	log.Println("websocket connected")

	c.conn = conn
	c.connected = true
	c.closed = false

	go c.waitMessage()

	return nil
}

func (c *Client) sendPing() {
	if !c.connected {
		return
	}
	err := c.sendMessage(websocket.TextMessage, []byte("ping"))
	if err != nil {
		log.Println("write ping failed", err)
		return
	}
}

func (c *Client) reconnect() {
	if c.closed {
		return
	}

	c.close()

	log.Println("websocket is disconnected, try reconnect...")

	for {
		if c.closed {
			break
		}
		if c.Connect() == nil {
			break
		}
	}
}

func (c *Client) keepAlive() {
	if !c.connected {
		return
	}
	if c.helloTimer == nil {
		c.helloTimer = time.AfterFunc(c.helloInterval, c.sendPing)
	} else {
		c.helloTimer.Reset(c.helloInterval)
	}

	if c.deadTimer == nil {
		c.deadTimer = time.AfterFunc(c.deadInterval, c.reconnect)
	} else {
		c.deadTimer.Reset(c.deadInterval)
	}
}

func KeysString(m map[string]struct{}) string {
	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	return strings.Join(keys, ",")
}
