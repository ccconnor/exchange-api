package internal

import (
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"

	"github.com/kraymond37/simplehttp"
	"github.com/mitchellh/mapstructure"
)

type Client struct {
	apiKey    string
	secretKey string
	client    *simplehttp.Client
}

func NewClient(endpoint, apiKey, secretKey string) *Client {
	c := simplehttp.NewClient(endpoint)
	if c == nil {
		fmt.Println("new client failed")
		return nil
	}
	return &Client{apiKey: apiKey, secretKey: secretKey, client: c}
}

func (c Client) RequestMap(method, path string, params map[string]interface{}, resp interface{}, needSign bool) (err error) {
	var header http.Header
	if needSign {
		header = MakeAuthHeader(c.apiKey, c.secretKey, method, path, params)
	}

	var body []byte
	switch method {
	case "GET":
		body, err = c.client.Get(path, params, header)
	case "POST":
		body, err = c.client.PostForm(path, params, header)
	case "PUT":
		body, err = c.client.PutForm(path, params, header)
	case "DELETE":
		body, err = c.client.DeleteForm(path, params, header)
	default:
		err = fmt.Errorf("method not supported")
	}
	if err != nil {
		fmt.Println(string(body))
		return err
	}

	err = json.Unmarshal(body, resp)
	if err != nil {
		return err
	}

	return nil
}

func (c Client) RequestStruct(method, path string, param, resp interface{}, needSign bool) error {
	var paramsMap map[string]interface{}
	if param != nil && !reflect.ValueOf(param).IsNil() {
		err := mapstructure.Decode(param, &paramsMap)
		if err != nil {
			return err
		}
	}

	return c.RequestMap(method, path, paramsMap, resp, needSign)
}
