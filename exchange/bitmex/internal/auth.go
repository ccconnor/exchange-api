package internal

import (
	"net/http"
	"net/url"
	"strconv"

	"github.com/kraymond37/simplehttp"
)

func MakeAuthHeader(apiKey, secretKey, method, path string, params map[string]interface{}) http.Header {
	header := http.Header{}
	if apiKey == "" || secretKey == "" {
		return header
	}

	var query string
	var payload string
	if method == "GET" {
		query = simplehttp.MapToUrlValues(params).Encode()
	} else {
		payload = simplehttp.MapToUrlValues(params).Encode()
	}
	reqURL := url.URL{Path: path, RawQuery: query}

	expires := GenerateNonce()
	signature := Sign(secretKey, method, reqURL.RequestURI(), expires, payload)

	header.Add("api-expires", strconv.FormatInt(expires, 10))
	header.Add("api-signature", signature)
	header.Add("api-key", apiKey)

	return header
}
