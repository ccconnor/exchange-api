package internal

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestGenerateNonce(t *testing.T) {
	fmt.Println(GenerateNonce())
}

func TestGenerateSignature1(t *testing.T) {
	secret := "chNOOS4KvNXR_Xq4k4c9qsfoKWvnDecLATCRlcBwyKDYnWgO"
	verb := "GET"
	path := "/realtime"
	nonce := int64(1580635350)
	signature := Sign(secret, verb, path, nonce, "")
	fmt.Println("signature:", signature)
}

func TestGenerateSignature2(t *testing.T) {
	secret := "ZTbKCf47aftV6PvoX7QFvU1EX6wsdzsAc7L-_K-JjHrIrxwv"
	verb := "POST"
	path := "/api/v1/position/leverage"
	nonce := int64(1580891470)

	params := map[string]interface{}{"symbol": "XBTUSD", "leverage": 1.0}
	data, _ := json.Marshal(params)
	signature := Sign(secret, verb, path, nonce, string(data))
	fmt.Println("signature:", signature)
}
