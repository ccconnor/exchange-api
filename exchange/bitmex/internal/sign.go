package internal

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"strconv"
	"time"
)

func GenerateNonce() int64 {
	return time.Now().Unix() + 3600
}

// Sign Generates an API signature.
//  A signature is HMAC_SHA256(secret, verb + path + nonce + data), hex encoded.
//  Verb must be uppercased, url is relative, nonce must be an increasing 64-bit integer
//  and the data, if present, must be JSON without whitespace between keys.
//
//  For example, in psuedocode (and in real code below):
//
//  verb=POST
//  uri=/api/v1/order
//  nonce=1416993995705
//  data={"symbol":"XBTZ14","quantity":1,"price":395.01}
//  signature = HEX(HMAC_SHA256(secret, 'POST/api/v1/order1416993995705{"symbol":"XBTZ14","quantity":1,"price":395.01}'))
func Sign(secret, verb, uri string, nonce int64, payload string) string {
	message := verb + uri + strconv.FormatInt(nonce, 10) + payload

	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(message))

	return hex.EncodeToString(h.Sum(nil))
}
