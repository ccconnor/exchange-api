package main

import (
	"encoding/json"
	"fmt"
	"gitee.com/ccconnor/exchange-api/exchange/bitmex/internal"
	"github.com/gorilla/websocket"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
)

func main() {
	msgTypeToGen := "trade"

	endpoint := "testnet.bitmex.com"
	apiKey := "cihb_hjaTRycwJ9ML4UEIGgR"
	secretKey := "ZTbKCf47aftV6PvoX7QFvU1EX6wsdzsAc7L-_K-JjHrIrxwv"
	u := url.URL{Scheme: "wss", Host: endpoint, Path: "/realtime", RawQuery: "subscribe=" + msgTypeToGen}

	header := make(http.Header)
	expires := internal.GenerateNonce()
	header.Add("api-expires", strconv.FormatInt(expires, 10))
	header.Add("api-signature", internal.Sign(secretKey, "GET", "/realtime", expires, ""))
	header.Add("api-key", apiKey)

	c, resp, err := websocket.DefaultDialer.Dial(u.String(), header)
	if resp != nil && resp.Body != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		fmt.Println("dial:", err)
		return
	}
	fmt.Println("connected to websocket")

	for {
		_, messageJSON, err := c.ReadMessage()
		if err != nil {
			fmt.Println("read:", err)
			break
		}

		var messageMap map[string]interface{}
		_ = json.Unmarshal(messageJSON, &messageMap)
		action, found := messageMap["action"]
		if !found || action != "partial" {
			fmt.Printf("recv: %s\n", messageJSON)
			continue
		}

		var keys []string
		for k := range messageMap["types"].(map[string]interface{}) {
			keys = append(keys, k)
		}
		sort.Strings(keys)

		fmt.Printf("type %s struct {\n", strings.Title(msgTypeToGen))
		for _, k := range keys {
			key := strings.Title(k)
			var typo string
			v := messageMap["types"].(map[string]interface{})[k]
			switch v {
			case "long":
				typo = "int64"
			case "symbol":
				typo = "string"
			case "symbols":
				typo = "[]string"
			case "float":
				typo = "float64"
			case "boolean":
				typo = "bool"
			case "timestamp":
				typo = "time.Time"
			case "guid":
				typo = "string"
			case "string":
				typo = "string"
			case "timespan":
				typo = "time.Time"
			default:
				typo = "Unknown"
				fmt.Printf("unknown %s's type(%s)\n", k, v)
			}
			fmt.Printf("\t%s %s `json:\"%s\"`\n", key, typo, k)
		}
		fmt.Println("}")

		break
	}
}
