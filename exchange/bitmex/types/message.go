package types

type Message struct {
	Action string      `json:"action"`
	Table  string      `json:"table"`
	Data   interface{} `json:"data"`
}
