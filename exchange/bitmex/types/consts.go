package types

const (
	TopicTrade         = "trade"
	TopicOrderBookL2   = "orderBookL2"
	TopicPosition      = "position"
	TopicExecution     = "execution"
	TopicOrderBookL225 = "orderBookL2_25"
	TopicOrder         = "order"
	TopicTradeBin1m    = "tradeBin1m"
	TopicMargin        = "margin"
)
