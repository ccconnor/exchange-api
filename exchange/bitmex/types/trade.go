package types

import "time"

type Trade struct {
	ForeignNotional float64   `json:"foreignNotional"`
	GrossValue      int64     `json:"grossValue"`
	HomeNotional    float64   `json:"homeNotional"`
	Price           float64   `json:"price"`
	Side            string    `json:"side"`
	Size            int64     `json:"size"`
	Symbol          string    `json:"symbol"`
	TickDirection   string    `json:"tickDirection"`
	Timestamp       time.Time `json:"timestamp"`
	TrdMatchID      string    `json:"trdMatchID"`
}

type TradeBin struct {
	Close           float64   `json:"close"`
	ForeignNotional float64   `json:"foreignNotional"`
	High            float64   `json:"high"`
	HomeNotional    float64   `json:"homeNotional"`
	LastSize        int64     `json:"lastSize"`
	Low             float64   `json:"low"`
	Open            float64   `json:"open"`
	Symbol          string    `json:"symbol"`
	Timestamp       time.Time `json:"timestamp"`
	Trades          int64     `json:"trades"`
	Turnover        int64     `json:"turnover"`
	Volume          int64     `json:"volume"`
	Vwap            float64   `json:"vwap"`
}
