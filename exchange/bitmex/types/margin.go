package types

import "time"

type Margin struct {
	Account            int64     `json:"account"`
	Action             string    `json:"action"`
	Amount             int64     `json:"amount"`
	AvailableMargin    int64     `json:"availableMargin"`
	Commission         float64   `json:"commission"`
	ConfirmedDebit     int64     `json:"confirmedDebit"`
	Currency           string    `json:"currency"`
	ExcessMargin       int64     `json:"excessMargin"`
	ExcessMarginPcnt   float64   `json:"excessMarginPcnt"`
	GrossComm          int64     `json:"grossComm"`
	GrossExecCost      int64     `json:"grossExecCost"`
	GrossLastValue     int64     `json:"grossLastValue"`
	GrossMarkValue     int64     `json:"grossMarkValue"`
	GrossOpenCost      int64     `json:"grossOpenCost"`
	GrossOpenPremium   int64     `json:"grossOpenPremium"`
	IndicativeTax      int64     `json:"indicativeTax"`
	InitMargin         int64     `json:"initMargin"`
	MaintMargin        int64     `json:"maintMargin"`
	MarginBalance      int64     `json:"marginBalance"`
	MarginBalancePcnt  float64   `json:"marginBalancePcnt"`
	MarginLeverage     float64   `json:"marginLeverage"`
	MarginUsedPcnt     float64   `json:"marginUsedPcnt"`
	PendingCredit      int64     `json:"pendingCredit"`
	PendingDebit       int64     `json:"pendingDebit"`
	PrevRealisedPnl    int64     `json:"prevRealisedPnl"`
	PrevState          string    `json:"prevState"`
	PrevUnrealisedPnl  int64     `json:"prevUnrealisedPnl"`
	RealisedPnl        int64     `json:"realisedPnl"`
	RiskLimit          int64     `json:"riskLimit"`
	RiskValue          int64     `json:"riskValue"`
	SessionMargin      int64     `json:"sessionMargin"`
	State              string    `json:"state"`
	SyntheticMargin    int64     `json:"syntheticMargin"`
	TargetExcessMargin int64     `json:"targetExcessMargin"`
	TaxableMargin      int64     `json:"taxableMargin"`
	Timestamp          time.Time `json:"timestamp"`
	UnrealisedPnl      int64     `json:"unrealisedPnl"`
	UnrealisedProfit   int64     `json:"unrealisedProfit"`
	VarMargin          int64     `json:"varMargin"`
	WalletBalance      int64     `json:"walletBalance"`
	WithdrawableMargin int64     `json:"withdrawableMargin"`
}
