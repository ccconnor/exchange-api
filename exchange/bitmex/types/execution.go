package types

import "time"

type Execution struct {
	Account               int64     `json:"account"`
	AvgPx                 float64   `json:"avgPx"`
	ClOrdID               string    `json:"clOrdID"`
	ClOrdLinkID           string    `json:"clOrdLinkID"`
	Commission            float64   `json:"commission"`
	ContingencyType       string    `json:"contingencyType"`
	CumQty                int64     `json:"cumQty"`
	Currency              string    `json:"currency"`
	DisplayQty            int64     `json:"displayQty"`
	ExDestination         string    `json:"exDestination"`
	ExecComm              int64     `json:"execComm"`
	ExecCost              int64     `json:"execCost"`
	ExecID                string    `json:"execID"`
	ExecInst              string    `json:"execInst"`
	ExecType              string    `json:"execType"`
	ForeignNotional       float64   `json:"foreignNotional"`
	HomeNotional          float64   `json:"homeNotional"`
	LastLiquidityInd      string    `json:"lastLiquidityInd"`
	LastMkt               string    `json:"lastMkt"`
	LastPx                float64   `json:"lastPx"`
	LastQty               int64     `json:"lastQty"`
	LeavesQty             int64     `json:"leavesQty"`
	MultiLegReportingType string    `json:"multiLegReportingType"`
	OrdRejReason          string    `json:"ordRejReason"`
	OrdStatus             string    `json:"ordStatus"`
	OrdType               string    `json:"ordType"`
	OrderID               string    `json:"orderID"`
	OrderQty              int64     `json:"orderQty"`
	PegOffsetValue        float64   `json:"pegOffsetValue"`
	PegPriceType          string    `json:"pegPriceType"`
	Price                 float64   `json:"price"`
	SettlCurrency         string    `json:"settlCurrency"`
	Side                  string    `json:"side"`
	SimpleCumQty          float64   `json:"simpleCumQty"`
	SimpleLeavesQty       float64   `json:"simpleLeavesQty"`
	SimpleOrderQty        float64   `json:"simpleOrderQty"`
	StopPx                float64   `json:"stopPx"`
	Symbol                string    `json:"symbol"`
	Text                  string    `json:"text"`
	TimeInForce           string    `json:"timeInForce"`
	Timestamp             time.Time `json:"timestamp"`
	TradePublishIndicator string    `json:"tradePublishIndicator"`
	TransactTime          time.Time `json:"transactTime"`
	TrdMatchID            string    `json:"trdMatchID"`
	Triggered             string    `json:"triggered"`
	UnderlyingLastPx      float64   `json:"underlyingLastPx"`
	WorkingIndicator      bool      `json:"workingIndicator"`
}
