# exchange-api

#### 使用方法
见[examples](examples)

#### 私有权限的仓库导入方法  
- 在`https://github.com/settings/tokens`生成一个token
- mac或linux系统编辑`$HOME/.netrc`, 新增下面一行, 替换`USERNAME`和`APIKEY`
  ```
  machine github.com login USERNAME password APIKEY
  ```
参考 https://golang.org/doc/faq#git_https  

##### 注意
如果使用了goproxy, goproxy有的不支持下载私有仓库, 如果`go get`失败就先取消goproxy, 下载私有库成功后再打开goproxy  

如果go>=1.13, 可直接如下设置
```
go env -w GOPROXY=https://goproxy.cn,direct
go env -w GOSUMDB=off
```